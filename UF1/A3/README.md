
#### Cristian Fernando Condolo Jimenez
#### 2ASIX EDT 2021-2022
#### M09-Implantación de aplicaciones web

## Actividades:

+ **Acticidad 1**: __Creación de una base de datos__. Crea una base de datos con PHPMyAdmin. La base se dira biblioteca i tendra dos tablas.

+ **Actividad 2**: __Creación de registros con la base de datos__. Crear nuevos registros a la base datos mediante un formulario. Crear un programa en PHP (en diferentes ficheros) que añadiremos libros a la base datos.

+ **Actividad 3**: __Importar y crear registros__. Importar una base de datos al PHPMyAdmin. Escoger una tabla de la base de datos, hacer un formulario con html para introducir registros en esta tabla.

## Extras:

+ **Prueba**: Examen anterior, se compone de 2 partes. 